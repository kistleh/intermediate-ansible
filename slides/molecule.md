### Molecule

[Online Documentation](https://molecule.readthedocs.io/en/latest/)


#### Testing Roles with Molecule

* <!-- .element: style="font-size: 18pt;" -->
  Meant to help while developing new Ansible roles
  * <!-- .element: style="font-size: 16pt;" -->
    *It supports the two latest major versions of Ansible (2.9 and 2.10 currently)*
* <!-- .element: style="font-size: 18pt;" -->
  A shared framework for writing consistent Ansible roles
* <!-- .element: style="font-size: 18pt;" -->
  Allows for testing a wide range of operating systems/distributions
* <!-- .element: style="font-size: 18pt;" -->
  Based on Python and available as a package (pip)
* <!-- .element: style="font-size: 18pt;" -->
  Similar to [vagrant](https://www.vagrantup.com/) but aimed at Ansible
* <!-- .element: style="font-size: 18pt;" -->
  Configuration is stored with the role under the `molecule/` directory inside the role

**But! You can also use it as a playground to learn more about Ansible**
<!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->


#### What can it test?

* <!-- .element: style="font-size: 18pt;" -->
  Molecule uses Ansible as its default verifier, but it also supports
  [Testinfra](https://testinfra.readthedocs.io/en/latest/) which comes with a
  set of built-in [modules](https://testinfra.readthedocs.io/en/latest/modules.html)
  * <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
    Verify that files exist with expected permissions and content
  * <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
    Verify services are enabled and running
  * <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
    Verify users and/or groups are present
  * <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
    Verify that packages, and specific package versions, are installed
  * <!-- .element: class="fragment" data-fragment-index="4" style="font-size: 18pt;" -->
    You can also run Ansible modules from within the test as well


#### Terminology

| Term              | Description                                                                            |
|:------------------|:---------------------------------------------------------------------------------------|
| Scenario          | _One or more test suites to apply for the role_                                        |
| Driver/Provider\* | _Defines the type of instance to create (docker, vagrant, etc.)_                       |
| Provisioner       | _How to run role tasks against instance_                                               |
| Converge          | _Apply role tasks as defined by the provisioner_                                       |
| Verifier          | _Framework to use for verifying instance state_                                        |
| Idempotency       | _Make sure that no tasks are showing up as <code style="color:orange;">changed</code>_ |
| Lint              | _Static code analysis for style/syntax checking_                                       |

_*Molecule supports any driver that Ansible supports_
<!-- .element: style="font-size: 14pt;" -->


#### Useful commands

| Command                | Description                                        |
|:-----------------------|:---------------------------------------------------|
| `molecule --help`      | _Get all available sub-commands_                   |
| `molecule init`        | _Create a new role/scenario_                       |
| `molecule list`        | _Output a list of all configured instances_        |
| `molecule create`      | _Create (start) instances associated with_         |
| `molecule converge`    | _Apply the Ansible role to the instances_          |
| `molecule idempotence` | _Verify that no unpexpected changes are happening_ |
| `molecule verify`      | _Run tests (testinfra) against the instances_      |
| `molecule login`       | _Login to a particular instance_                   |
| `molecule destroy`     | _Cleanup_                                          |


#### Exercise: Installation

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  [Demo](https://galaxy.ansible.com/kistleh/nginx)
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  Installation
  ```bash
  # Install Python dependencies
  sudo apt update && sudo apt install python3-dev python3-venv libssl-dev -y

  # Create a new virtual environment
  python3 -m venv ~/.venvs/molecule
  source ~/.venvs/molecule/bin/activate

  # Install Molecule (which will also install Ansible)
  pip install --upgrade pip
  pip install --upgrade setuptools
  pip install wheel
  pip install "molecule[ansible,docker,lint]"

  # Check version
  molecule --version
  mol --version
  ansible --version

  # Install our skeleton role
  cd $INTERMED_ANSIBLE_DIR/ansible-roles
  ansible-galaxy install kistleh.nginx -p .
  cd kistleh.nginx
  ```
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  Familiarize yourself with the role layout - Do you see anything missing?


#### Exercise: Getting started

<div style="width: 40%; float: left; font-size: 18pt;">
  <pre class="fragment" data-fragment-index="0"><code data-trim>
    # Initialize a new default scenario with Testinfra as the verifier
    molecule init scenario -d docker -r kistleh.nginx --verifier-name testinfra default

    # List instances
    molecule list
  </code></pre>
  <pre class="fragment" data-fragment-index="1"><code data-trim>
    ---
    # This is so we don't have to duplicate it for every platform
    default_platform: &platform
      privileged: true
      tmpfs:
        - /run
        - /tmp
      volumes:
        - "/sys/fs/cgroup:/sys/fs/cgroup:ro"
      command: /lib/systemd/systemd
      tty: true
      environment:
        container: docker
    platforms:
      - name: default-ubuntu1804
        image: litmusimage/ubuntu:18.04
        published_ports:
          - "127.0.0.1:10004:80/tcp"
        <<: *platform
  </code></pre>
  <pre class="fragment" data-fragment-index="2"><code data-trim>
    # Verify instance name has changed and create it
    molecule list
    molecule create
    molecule login --host default-ubuntu1804
  </code></pre>
</div>

<div style="width: 60%; float: left; font-size: 14pt;">
  <ul>
    <li class="fragment" data-fragment-index="0">Initialize new scenario</li>
    <li class="fragment" data-fragment-index="1">Update configuration file <code>molecule/default/molecule.yml</code></li>
    <li class="fragment" data-fragment-index="2">Verify instance name has changed and create it</li>
    <li class="fragment" data-fragment-index="3">On your own</li>
    <ul>
      <li class="fragment" data-fragment-index="3">Apply the role and verify you can visit http://localhost:10004</li>
      <li class="fragment" data-fragment-index="3">Verify the role's idempotency (find and fix the bug)</li>
      <li class="fragment" data-fragment-index="3">Tip: <code>molecule --help</code></li>
    </ul>
    <li class="fragment" data-fragment-index="4">Run <code>molecule destroy</code> when you're done</li>
  </ul>
<div>


#### Exercise: Verify role syntax

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  ```yaml
  # .yamllint
  ---
  extends: default

  rules:
    line-length:
      max: 120
    new-lines:
      type: unix
  ```
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  ```ini
  ; .flake8
  [flake8]
  max-line-length = 120
  ```
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  ```yaml
  # molecule/default/molecule.yml
  ...
  lint: |
    set -e
    yamllint .
    ansible-lint .
    flake8
  ...
  ```
* <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
  ```bash
  # Check and fix any role lint/syntax issues
  molecule syntax
  molecule lint
  ```


#### Exercise: Add CentOS/Ubuntu instances

* <!-- .element: style="font-size: 18pt;" -->
  Update configuration file `molecule/default/molecule.yml`
  ```yaml
  platforms:
    - name: default-centos7
      image: litmusimage/centos:7
      published_ports:
        - "127.0.0.1:10002:80/tcp"
      <<: *platform

    - name: default-centos8
      image: litmusimage/centos:8
      published_ports:
        - "127.0.0.1:10003:80/tcp"
      <<: *platform

    - name: default-ubuntu1804
      image: litmusimage/ubuntu:18.04
      published_ports:
        - "127.0.0.1:10004:80/tcp"
      <<: *platform

    - name: default-ubuntu2004
      image: litmusimage/ubuntu:20.04
      published_ports:
        - "127.0.0.1:10005:80/tcp"
      <<: *platform
  ```
* <!-- .element: style="font-size: 18pt;" -->
  You should have 4 instances when you're done (`molecule list`)


#### Exercise: Update role for CentOS/Ubuntu

Modify the role to work on both CentOS and Ubuntu
<!-- .element: style="font-size: 18pt;" -->

<div style="width: 50%; float: left; font-size: 18pt;"><pre>
.
├── tasks
│   ├── centos.yml
│   ├── <mark class="fragment" data-fragment-index="1">main.yml</mark>
│   └── ubuntu.yml
└── vars
    ├── CentOS.yml
    ├── main.yml
    └── Ubuntu.yml
</pre></div>

<div class="fragment" data-fragment-index="1" style="width: 50%; float: left; font-size: 18pt;"><pre><code data-trim>
- include_vars:
    file: "{{ ansible_facts.distribution }}.yml"

- import_tasks: centos.yml
  when: ansible_facts.distribution == "CentOS"

- import_tasks: ubuntu.yml
  when: ansible_facts.distribution == "Ubuntu"
</code></pre></div>

_Hint: The installation method and the user/group are **unique** between CentOS and Ubuntu_
<!-- .element: class="fragment" data-fragment-index="2" style="font-size: 14pt;" -->

<div class="fragment" data-fragment-index="3" style="width: 50%; float: left; font-size: 18pt;"><pre><code data-trim data-noescape>
- name: Install EPEL Repository
  yum:
    name: "https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_facts.distribution_major_version }}.noarch.rpm"
    state: present
    disable_gpg_check: true
</code></pre></div>

<div class="fragment" data-fragment-index="4" style="width: 50%; float: left; font-size: 14pt;">
  <ul>
    <li><a href="http://localhost:10002">CentOS 7</a></li>
    <li><a href="http://localhost:10003">CentOS 8</a></li>
    <li><a href="http://localhost:10004">Ubuntu 18.04</a></li>
    <li><a href="http://localhost:10005">Ubuntu 20.04</a></li>
  </ul>
</div>


#### Solution: Demo

* Task layout (pay attention to task names)
* `meta/main.yml`


#### Testinfra

* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  Unit tests for servers
  * Written in Python as a plugin to Pytest
  * Tests are written in Python as well
  * [Online Documentation](https://testinfra.readthedocs.io/en/latest/)
* <!-- .element: class="fragment" data-fragment-index="1" style="font-size: 18pt;" -->
  Tests defined in `molecule/<scenario>/tests/test_*.py`
  * Molecule default test at `molecule/<scenario>/tests/test_default.py`
* <!-- .element: class="fragment" data-fragment-index="2" style="font-size: 18pt;" -->
  A Python function for each type of test that starts with `test_`
* <!-- .element: class="fragment" data-fragment-index="3" style="font-size: 18pt;" -->
  Demo


#### Exercise: Testinfra

* <!-- .element: style="font-size: 14pt;" -->
  Install Testinfra
  ```bash
  pip install pytest-testinfra
  ```
* <!-- .element: style="font-size: 14pt;" -->
  Update `molecule/default/tests/test_default.py` (indentation important)
  ```
  """Role testing files using testinfra."""
  import os
  import sys

  def test_nginx_is_installed(host):
      nginx = host.package("nginx")

      assert nginx.is_installed
  ```

* <!-- .element: style="font-size: 14pt;" -->
  Update `molecule/default/molecule.yml` to be a bit more verbose
  ```
  verifier:
    name: testinfra
    options:
      verbose: true
  ```
* <!-- .element: style="font-size: 14pt;" -->
  Verify that the test pass with `molecule verify`
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 14pt;" -->
  Write a new test to verify that Nginx is running and enabled to start at boot
  ```
  def test_nginx_is_running_and_enabled(host):
      # Tests goes here...
  ```


#### Challenge 1: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function named `test_nginx_configuration` that verifies that
  the Nginx configuration file (`/etc/nginx/nginx.conf`):
  * Exists
  * Is a file
  * Owned by the `root` user
  * Owned by the `root` group
  * Has _any_ expected content (use `molecule login` to check)
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Challenge 2: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function named `test_nginx_default_site` that verifies that
  the default site (`/etc/nginx/conf.d/default.conf`):
  * Exists
  * Is a file
  * Owned by `www-data` user/group on Ubuntu systems
  * Owned by `nginx` user/group on CentOS systems
  * Permissions are set to `640`
  * Has _any_ expected content (use `molecule login` to check)
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Challenge 3: Testinfra

* <!-- .element: style="font-size: 18pt;" -->
  Write a new test function that verifies:
  * Nginx is listening on `0.0.0.0:80`
  * Visiting `http://127.0.0.1:80` returns 200
  * Visiting `http://127.0.0.1:80` returns expected OS content
* <!-- .element: style="font-size: 18pt;" -->
  Check out the [online documentation](https://testinfra.readthedocs.io/en/latest/)
  for help


#### Solution: Testinfra Challenges

* _A solution_
* Full test sequence with `molecule test`


#### Exercise: Ansible Verifier

* <!-- .element: style="font-size: 18pt;" -->
  Initialize a new scenario with `ansible` as the verifier
  ```
  molecule init scenario -d docker -r kistleh.nginx --verifier-name ansible ansible
  molecule list
  molecule create -s ansible
  molecule login
  ```
* <!-- .element: class="fragment" data-fragment-index="0" style="font-size: 18pt;" -->
  On your own
  * Destroy the instance and setup Ubuntu/CentOS instances for the `ansible` scenario
  * Create and converge all instances
  * Re-write the tests you've written with Testinfra to Ansible
    * Use `molecule/ansible/verify.yml`
  * Verify there are no outstanding syntax/lint issues


#### Demo: Ansible Galaxy

* <!-- .element: style="font-size: 18pt;" -->
  Commit changes to GitHub repository
* <!-- .element: style="font-size: 18pt;" -->
  Create a new tag
* <!-- .element: style="font-size: 18pt;" -->
  Re-import role to Ansible Galaxy
  * It should now support multiple operating systems
