## Course Outline


* [Organising Infrastructure Code](#/3)
    * Including playbooks
    * Including tasks
    * Passing variables to includes
    * Blocks


* [Task conditions](#/4)
  * Interpreting and controlling errors
    - ignoring errors
  * Manipulating task conditions
    - Errors
    - Changed state
  * Error recovery
    - block/rescue


* [Roles part 2](#/5)
  * Importing roles
  * Distributing your own roles


* [Testing Ansible](#/6)
  * Molecule
  * Testinfra


<!--* [Deploying code](deploying-code.md)-->
  <!--*  Deploying loadbalanced applications-->
  <!--*  Ansible via a bastion host-->

<!--* [Set Theory in Ansible](group-set-theory.md)-->
  <!--*  Set theory filters-->
  <!--*  Inventory set theory operators-->

<!--* [Upgrade strategies](upgrade-strategies-pt1.md)-->
  <!--*  What can go wrong?-->
  <!--*  Types of strategies-->

<!--* [Dynamic Inventories](dynamic-inventories.jd)-->
<!--* [Handling Failure](failing-fast.md)-->

<!--* [Rolling Upgrade Demo](rolling-upgrade-demo.md)-->
* [Wrap Up](#/7)
